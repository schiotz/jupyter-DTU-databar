#!/bin/bash

# This script should be source'd by the students to set up
# their accounts.  Use this command:
#
# source ~jasc/setup10031
#


if [[ "`uname`" != "Linux" ]]; then
    echo "ERROR: Should be run on a Linux machine, not `uname`"
    exit 1
fi

echo
echo
echo "SETTING UP DATABAR ACCOUNT FOR USER" $USER
echo

# Set up .bashrc
cd
grep -q 'source ~jasc/10031F18.bashrc' .bashrc
if [[ $? -eq 0 ]]; then
    echo "Your .bashrc file is already set up.  Good."
else
    echo "Setting up your .bashrc file."
    echo "" >> .bashrc
    echo "# The line below was added by course 10031 setup script." >> .bashrc
    echo "source ~jasc/10031F18.bashrc" >> .bashrc
    echo "" >> .bashrc
    source ~jasc/10031F18.bashrc
fi

# Install Jupyter and nglview - need to be installed by the user
# otherwise the nglview extension will not work. :-(

echo
echo "Installing Jupyter and nglview in 3 seconds."
echo
sleep 3
python3 -m pip install jupyter nglview --user
if [[ $? -eq 0 ]]; then
    echo
    echo "Jupyter and nglview are installed."
    echo
else
    echo
    echo 'INSTALLATION FAILED - ASK FOR HELP !'
    echo
    exit 1
fi

# Make a folder for Notebook files.

echo
if [[ -d 10031 ]]; then
    echo "The folder 10031 (for your Jupyter Notebooks) already exists.  Good."
else
    echo "Creating folder name '10031' for your Jupyter Notebooks."
    mkdir 10031
fi

# Copy the Jupyter Notebooks

SOURCEDIR=~jasc/10031F18
SOURCEBOOKS=`(cd $SOURCEDIR && ls *.ipynb)`
#echo FILES: $SOURCEBOOKS

for FILE in $SOURCEBOOKS; do
    if [[ -f 10031/$FILE ]]; then
	echo "Notebook file $FILE is already present."
    else
	echo "Copying file $FILE to folder 10031."
	cp $SOURCEDIR/$FILE 10031
    fi
done

echo
if [[ -d .jupyter ]]; then
    echo "Jupyter Notebook configuration folder (.jupyter) found."
else
    echo "Creating Jupyter Notebook default configuration files."
    jupyter notebook --generate-config
fi

echo "Enabling Jupyter Notebook extensions."
jupyter-nbextension enable nglview --py 
jupyter nbextension enable widgetsnbextension --py


# Set up a Jupyter Notebook password.
PSWDFILE=.jupyter/jupyter_notebook_config.json
if [[ -f $PSWDFILE ]]; then
    grep -q password $PSWDFILE
    if [[ $? -eq 0 ]]; then
	NEEDPSWD=0
    else
	NEEDPSWD=1
    fi
else
    NEEDPSWD=1
fi
if [[ $NEEDPSWD -ne 0 ]]; then
    echo
    echo
    echo "IMPORTANT: You should now set a password you use to access your"
    echo "    Jupyter Notebooks.  You should probably NOT use your usual"
    echo "    DTU password (never give it to random programs aksing for it!)"
    echo "    Choose a new password."
    echo
    jupyter notebook password
    echo
    
    # Test if it worked
    grep -q password $PSWDFILE
    if [[ $? -eq 0 ]]; then
	echo "Your password was set.  To change it later, run this command:"
	echo "    jupyter notebook password"
    else
	echo "ERROR: Setting the password FAILED"
	echo "    Please run this script again."
	exit 1
    fi
    echo
else
    echo
    echo "You already have a Jupyter Notebook password.  Good."
    echo "If you need to change it, run the command"
    echo "    jupyter notebook password"
    echo
fi

echo "You are now ready to run the simulations!"


    
