
module load python3
module load gcc/6.4.0

COURSEHOME=/zhome/9f/f/2737/10031F18
# Stuff installed with pip
export PATH=$HOME/.local/bin:$COURSEHOME/bin:$PATH
export PYTHONPATH=$COURSEHOME/lib/python3.6/site-packages:$PYTHONPATH

# The Atomic Simulation Environment
export PYTHONPATH=$COURSEHOME/src/ase:$PYTHONPATH

# ASAP
export PYTHONPATH=$COURSEHOME/src/asap/build/lib.linux-x86_64-3.6:$PYTHONPATH

#PyQSTEM
export PYTHONPATH=$COURSEHOME/src/PyQSTEM/build/lib.linux-x86_64-3.6:$PYTHONPATH
