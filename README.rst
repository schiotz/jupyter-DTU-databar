======================================
Jupyter Notebooks in the DTU G-Databar
======================================

This project documents how to run the Atomic Simulation Environment (ASE) in Jupyter Notebooks in the DTU databar in a practical way, with a focus on how to set up the system.  The project includes scripts to set up student accounts and a guide for the students.

.. contents::
   

Files and folders in this project
=================================

README.rst
  This file.  This is the main documentation.

Databar_setup_DK.pdf
  Guide for the students (in Danish).  This was written for the course 10031 Introduction to Physics and Nanotechnology (on the first year of the Physics and Nanotechnology bachelor education).

Databar_setup
  Folder with the LaTeX source for the guide.

10031F18.bashrc
  Sample file to be sourced by the students in their ``.bashrc`` file.  From course 10031.

setup10031
  Shell script to be sourced by the students to set up their account.

simulate10031
  Shell script starting Jupyter Notebook with the right command line
  options.

Notebooks_course_10031
  Sample notebooks from the course 10031.  Mainly to demonstrate how
  you can visualize in a notebook.
  
  
The problem
===========

* The G-Databar consists of a few login nodes, and a number of computational nodes.  Some computational nodes are for batch jobs, other for interactive jobs.

* The students need to run the Jupyter Notebooks on the interactive compute nodes, but the browser should run on their own laptop for performance reason.

* The compute nodes are not accessible from the outside network, so a SSH tunnel must be established to the running Jupyter server.

* *Additional challenge:* The interactive NGLviewer (for displaying ASE atoms / trajectories) only works if the students install it themselves, not if it is installed by the teacher.

  
Guidelines
==========

* Python 3 is available in the databar (must be loaded as a module), but no external packages are installed.

* The teacher installs most Python packages needed by the student, including the scientific software (ASE, GPAW, ASAP etc) and the prerequisites for Jupyter Notebook, but *not Jupyter Notebook itself*, which must be installed by the student for extensions to work properly.

  - Most of the software is installed with pip into ~/.local and then moved to a course specific folder so the installation is not messed up by installing something else later.

  - ASE and GPAW could be installed as "developer versions" to allow fixing bugs discovered by the students.

* A file is provided that the students can source from their .bashrc to get access to all of this.

* A script is provided to help the students set up their own account.  It

  - sets up .bashrc

  - installs Jupyter Notebook

  - enables the nglview extension

  - sets a Jupyter Notebook password for remote access

  - copies template Notebooks to a course folder under the student's home folder


Setting up the Teacher's account
================================

Installation must be done at one of the compute nodes.  It looks like some python modules are available on the login node only, so if installing there they would not be installed properly.

Log into the G-bar, and then log into an interactive node using the command::

  linuxsh -X

(The ``-X`` option enables X11 forwarding, and is of course optional).

* If you already have stuff installed in ``.local`` you should probably delete or rename that folder to spare the students from your old junk :-)

* Make sure that you do not have PYTHONPATH set to anything in your own folder, in particular own versions of numpy, ase or gpaw would cause pip to skip installing these packages.

Now we are ready to install!


1. Load Python 3 and GCC version 6.4.0 (the default GCC is archaic and the newest caused trouble for me)::

     module load python3
     module load gcc/6.4.0

#. Install NumPy and friends.  **IMPORTANT:** We need to get the version of pip corresponding to our python3 installation, hence the use of ``python3 -m pip`` ::

     python3 -m pip install --user numpy scipy matplotlib

#. If PyQSTEM is needed, we also need these prerequisites::

     python3 -m pip install --user scikit-image Pillow Cython

#. User installation of Jupyter Notebook is a *lot* faster if we install these prerequisites::

     python3 -m pip install --user traitlets ptyprocess tornado terminado MarkupSafe jinja2 Send2Trash jsonschema nbformat pygments webencodings html5lib bleach entrypoints pandocfilters mistune testpath nbconvert pyzmq wcwidth prompt-toolkit==1.0.15 pexpect pickleshare simplegeneric parso jedi backcall

   **REMEMBER:** Do **not install** jupyter itself, as that will break nglview for the students.

#. (Optional) Install ase and gpaw if you do not desire a developer installation.

#. Copy all the stuff we just installed into a safe place where the students can access it.  Then set permissions so they can read the files (a few files have too restrictive permissions when installed by pip)::

     cd COURSEFOLDER
     rsync -av ~/.local/. .
     find . -type d -exec chmod og+rx {} +
     find . -type f -exec chmod og+r {} +

#. Install any developer versions in their own folders under COURSEFOLDER.

#. Provide a file the students can source from their .bashtc, see the example in this project.

   
Setting up the student accounts
===============================

The students need to do the following (can be done in a script, see setup10031)

1. Add this line to their ``.bashrc``::

     source ~teacher/10031F18.bashrc

   (adapt the name, of course)

#. Activate it::

     source ~/.bashrc

#. Install Jupyter and nglview::

     python3 -m pip install --user jupyter nglview

#. Generate a default Jupyter configuration file/folder::

     jupyter notebook --generate-config

#. Activate the viewer extensions to Jupyter (this is the step that fails if Jupyter was installed by the teacher)::

     jupyter nbextension enable nglview --py 
     jupyter nbextension enable widgetsnbextension --py

#. Set a Jupyter password::

     jupyter notebook password

     
Starting the Jupyter Notebook server
====================================

The students should log in to the machine ``login.gbar.dtu.dk`` using
SSH (command line on Mac/Linux, using MobaXterm on Windows).  They
should enable X11 forwarding, as otherwise ``ase gui`` will not work;
this is automatic when using MobaXterm, but Linux/Mac users should add
the ``-X`` option to ssh::

  ssh -X USERNAME@login.gbar.dtu.dk
  
Once logged in, the students should proceed to one of the interactive compute nodes::

  linuxsh -X
  
To start the notebook server, the students should cd to the folder
with the notebooks, and run the command::

  jupyter notebook --no-browser --port=40000 --ip=$HOSTNAME

NOTES:

* This should be done as a script checking that they are on the
  compute node, not on ``gbarlogin``, and probably also check that
  $DISPLAY is set correctly.
  
* On the databar, $HOSTNAME is automatically set to the name of the host where you are logged in.  This option causes jupyter to listen to connections on its ethernet network instead of on localhost only.

* Although you ask for port 40000, somebody else may have taken it already.  You will then be assigned another port close to this one.

Look at the output from the server, you should see something like this::

    jupyter notebook --no-browser --port=40000 --ip=$HOSTNAME
    [I 10:56:58.018 NotebookApp] Serving notebooks from local directory: /Users/schiotz/development/jupyter-DTU-databar
    [I 10:56:58.018 NotebookApp] 0 active kernels
    [I 10:56:58.018 NotebookApp] The Jupyter Notebook is running at:
    [I 10:56:58.018 NotebookApp] http://n-62-27-18:40000/
    [I 10:56:58.018 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).

Note the hostname and port number in the second-to-last line (here n-62-27-18 and 40000).

Connecting to the Jupyter Notebook server
=========================================

It is necessary to set up an SSH tunnel forwarding a port (typically por 8080) from the local machine to the interactive node to the compute node via the login server.

**On a Mac or Linux machine**, use the command::

  ssh USERNAME@l1.hpc.dtu.dk -g -L8080:HOSTNAME:PORT -N
   
where USERNAME is the student's username, and HOSTNAME:PORT are taken from the output of ``jupyter notebook``, here n-62-27-18:40000

*NOTE:* G-bar support told me to use ``l1.hpc.dtu.dk`` as jump node.  It looks like ``login.gbar.dtu.dk`` works as well, but I will investigate (and confer with GBAR support) and then update this document.


**On a windows machine**, use the Tunnel dialog in MobaXterm, see the PDF document.


**Finally,** open a browser, and point it to http://localhost:8080 

Testing the setup
=================

To test the setup, go to the web page https://guest.dtu.dk and create
a guest account.  Remember to give it a password after creating it.

You can now log in to the test account, and follow the instructions
for setting up the student accounts (above, or on the GPAW wiki page).

If the tests fail and you need a pristine account for further testing,
delete the ``.local`` and ``.jupyter`` folders.

